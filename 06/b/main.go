package main

import (
	"bufio"
	"log"
	"os"
)

var score int
var unrepeated int
var elems map[rune]int

func main() {
	file, err := os.Open("./input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := string(scanner.Text())
		for i := 14; i < len(line); i++ {
			elems = make(map[rune]int)
			for _, v := range line[i-14 : i] {
				elems[v] += 1
				if elems[v] > 1 {
					unrepeated = 0
					break
				} else {
					unrepeated += 1
				}
			}
			if unrepeated == 14 {
				score = i
				break
			}
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	log.Println(score)
}
