package main

import (
	"bufio"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var score int

var readStateStore, workStateStore, readMoveStore, workMoveStore [][]string

type elems map[int]bool

func main() {
	file, err := os.Open("./input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	mode := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := string(scanner.Text())
		if mode == 0 {
			if v, _ := regexp.MatchString("^ *$", line); v {
				mode = 1
			} else {
				for i := 4; i < len(line); i += 4 {
					line = line[:i-1] + "," + line[i:]
				}
				readStateStore = append(readStateStore, strings.Split(line, ","))
			}
		} else {
			readMoveStore = append(readMoveStore, strings.Split(line, " "))
		}
	}
	workStateStore = make([][]string, len(readStateStore[0]))
	for len(readStateStore) > 0 {
		n := len(readStateStore) - 1 // Top element
		for index, item := range readStateStore[n] {
			if match, _ := regexp.MatchString(" \\d ", item); item != "   " && !match {
				workStateStore[index] = append(workStateStore[index], item)
			}

		}
		readStateStore = readStateStore[:n] // Pop
	}
	log.Println("Starting da moves")
	log.Println(len(workStateStore))
	workMoveStore := readMoveStore
	for _, v := range workStateStore {
		log.Println(v)
	}
	for _, v := range workMoveStore {
		from, err := strconv.Atoi(v[3])
		to, err := strconv.Atoi(v[5])
		amount, err := strconv.Atoi(v[1])
		from, to = from-1, to-1
		log.Println("Moving ", amount, "from", from, "to", to)
		if err != nil {
			log.Fatal(err)
		}
		for i := 0; i < amount; i++ {
			n := len(workStateStore[from]) - 1 // Top element
			workStateStore[to] = append(workStateStore[to], workStateStore[from][n])
			workStateStore[from] = workStateStore[from][:n] // Pop
		}

		for _, v := range workStateStore {
			log.Println(v)
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	for _, v := range workStateStore {
		log.Println(v[len(v)-1])
	}
}
