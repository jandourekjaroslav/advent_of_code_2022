# Advent of Code 2022

A set of solutions to the Advent of Code challenges for 2022 written in Go.

Each Challenge is split into 2 parts.

Inputs and solutions are split into folders by day and part.

[Advent of Code 2022](https://adventofcode.com/2022)

# Experience so far

Fun little set of challenges

Challenges consist of mainly simple data parsing and finding basic patterns like overlaps and counts.

Let's see if it gets more tricky as days go on (end of day 4)

---

Day 5, the most annoying challenge so far. Simple enough to solve once one gets past the horrid way data is presented on input.
The quickest solve would definitely be to change format of initial state in input file itself. Moves are trivial to parse after that.
Did it the hard way with a reversing list and string substitutions in Go instead of just bashing a couple of Visual Block edits in VIM and inverting the order like a sane person would.
Part 2 of the task was rather trivial to change over to

Day 6, back to fun and quick. A simple rolling window of defined size takes care of both parts, yay for Go slices. Not the most elegant solution given wast majority of elements are visited the same amount of times as the window size. A simple map from which the first element of the window gets deleted at the end of the check would probably be far quicker.
Part 2 was "change and number and rerun" wish it was more involved like decoding messages with ROT-13 or something

Day 7, Objects HOOOOOO, or structs more precisely cause Go and they'll do. The most fun task so far, more prep work of the data structure with an N-ary tree style than computation only to quickly fill it and play with the resulting abstraction of your own making. Chef's kiss \*
Throw a bit of recursion in there for good measure and i have myself a very pleasant rainy Sunday afternoon hour of light coding. More of tasks like this would be Aces by me.

# License

Copyright (c) <2023> <copyright Jaroslav Jandourek>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
