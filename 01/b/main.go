package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
)

var current int

func main() {
	highest := [3]int{0, 0, 0}
	file, err := os.Open("./input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			if current > highest[0] {
				if current > highest[1] {
					if current > highest[2] {
						highest[0] = highest[1]
						highest[1] = highest[2]
						highest[2] = current
					} else {
						highest[0] = highest[1]
						highest[1] = current
					}
				} else {
					highest[0] = current
				}
			}
			current = 0
		} else {
			i, err := strconv.Atoi(line)
			if err != nil {
				log.Fatal(err)
			} else {
				current += i
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	log.Println(highest[0] + highest[1] + highest[2])

}
