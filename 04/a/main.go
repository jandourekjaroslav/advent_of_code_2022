package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

var score int

type elems map[int]bool

func main() {
	file, err := os.Open("./input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		splits := strings.Split(line, ",")
		left := strings.Split(splits[0], "-")
		right := strings.Split(splits[1], "-")
		left_low, err := strconv.Atoi(left[0])
		if err != nil {
			// ... handle error
			panic(err)
		}
		left_high, err := strconv.Atoi(left[1])
		if err != nil {
			// ... handle error
			panic(err)
		}
		right_low, err := strconv.Atoi(right[0])
		if err != nil {
			// ... handle error
			panic(err)
		}
		right_high, err := strconv.Atoi(right[1])
		if err != nil {
			// ... handle error
			panic(err)
		}
		if left_low <= right_low && left_high >= right_high {
			score += 1
		} else if right_low <= left_low && right_high >= left_high {
			score += 1
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	log.Println(score)

}
