package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

var score int

type elems map[int]bool

func main() {
	file, err := os.Open("./input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		left, right := line[:len(line)/2], line[len(line)/2:]
		leftMap := make(elems)
		rightMap := make(elems)
		for _, v := range left {
			leftMap[int(v)] = true
		}
		for _, v := range right {
			if leftMap[int(v)] && !rightMap[int(v)] {
				if int(v) < 91 {
					score += int(v) - 64 + 26
					fmt.Println(string(v), int(v)-64+26)
				}
				if int(v) > 96 && int(v) < 123 {
					score += int(v) - 96
					fmt.Println(string(v), int(v)-96)
				}
				rightMap[int(v)] = true
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	log.Println(score)

}
