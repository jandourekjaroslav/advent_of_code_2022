package main

import (
	"bufio"
	"log"
	"os"
)

var score, counter int

type elems []map[int]bool

func main() {
	file, err := os.Open("./input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	maps := elems{make(map[int]bool), make(map[int]bool), make(map[int]bool)}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if counter < 2 {
			for _, v := range line {
				maps[counter][int(v)] = true
			}
			counter += 1
		} else {
			for _, v := range line {
				if maps[0][int(v)] && maps[1][int(v)] && !maps[2][int(v)] {
					if int(v) < 91 {
						score += int(v) - 64 + 26
					}
					if int(v) > 96 && int(v) < 123 {
						score += int(v) - 96
					}
					maps[2][int(v)] = true
				}
			}
			counter = 0
			maps = elems{make(map[int]bool), make(map[int]bool), make(map[int]bool)}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	log.Println(score)

}
