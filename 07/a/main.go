package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

var mode int

type File struct {
	size int
	name string
}
type Directory struct {
	parent      *Directory
	files       []*File
	directories []*Directory
	size        int
	name        string
}

func newFile(size int, name string) File {
	nFile := File{}
	nFile.size = size
	nFile.name = name
	return nFile
}
func newDirectory(parent *Directory, name string) *Directory {
	nDirectory := Directory{}
	nDirectory.size = 0
	nDirectory.files = nil
	nDirectory.name = name
	nDirectory.directories = nil
	if parent == nil {
		nDirectory.parent = nil
	} else {
		nDirectory.parent = parent
	}
	return &nDirectory
}
func computeDirectorySize(dir *Directory) {
	for _, v := range dir.files {
		dir.size += v.size
	}
	for _, v := range dir.directories {
		computeDirectorySize(v)
		dir.size += v.size
	}
}

func findAllDirectorieswithMaxSize(root *Directory, maxSize int) []Directory {
	dirList := []Directory{}

	for _, v := range root.directories {
		log.Println("Directory size is", v.size, "the limit is", maxSize)
		if v.size <= maxSize {
			dirList = append(dirList, *v)
		}
		dirList = append(dirList[:], findAllDirectorieswithMaxSize(v, maxSize)[:]...)
	}
	return dirList
}
func getTotalSize(dirList []Directory) int {
	size := 0
	for _, v := range dirList {
		size += v.size
	}
	return size
}

func main() {
	file, err := os.Open("./input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	var currentDirectory *Directory
	var rootDirectory *Directory
	log.Println("Creating root")
	rootDirectory = newDirectory(nil, "/")
	log.Println("Root created")
	for scanner.Scan() {
		line := string(scanner.Text())
		if line[0] == '$' {
			mode = 1
			log.Println("Command mode entered")
		} else {
			mode = 0
			log.Println("Command mode exited")
		}
		switch mode {
		case 1:
			command := "" + string(line[2]) + string(line[3])
			log.Println("Command is ", command)
			if command == "cd" {
				argList := strings.Split(line, " ")
				arg := argList[len(argList)-1]
				log.Println("Arg is ", arg)
				switch arg {
				case "/":
					log.Println("Switched current dir to Root")
					currentDirectory = rootDirectory
				case "..":
					currentDirectory = currentDirectory.parent
					log.Println("Switched current dir to parent with name", currentDirectory.name)
					break
				default:
					child := newDirectory(currentDirectory, arg)
					currentDirectory.directories = append(currentDirectory.directories, child)
					currentDirectory = child
					log.Println("Switched current dir child ", currentDirectory.name, "which was created under", currentDirectory.parent.name)
					break
				}
			} else {
				mode = 0
				log.Println("Command mode exited")
			}
			break
		case 0:
			def := strings.Split(line, " ")
			if def[0] != "dir" {
				size, err := strconv.Atoi(def[0])
				if err != nil {
					log.Fatal(err)
				}
				file := newFile(size, def[1])
				currentDirectory.files = append(currentDirectory.files, &file)
				log.Println("Adding file ", def[1], "with size ", size, "to ", currentDirectory.name)
			}
			break
		default:
			break
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	computeDirectorySize(rootDirectory)
	log.Println(getTotalSize(findAllDirectorieswithMaxSize(rootDirectory, 100000)))
}
