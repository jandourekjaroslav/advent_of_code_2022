package main

import (
	"bufio"
	"log"
	"os"
)

var score int

func main() {
	file, err := os.Open("./input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		switch string(line[0]) {
		case "A":
			switch string(line[2]) {
			case "X":
				score += 3
			case "Y":
				score += 1 + 3
			case "Z":
				score += 2 + 6
			default:
				log.Fatal("Cheater!")
			}
		case "B":
			switch string(line[2]) {
			case "X":
				score += 1
			case "Y":
				score += 2 + 3
			case "Z":
				score += 3 + 6
			default:
				log.Fatal("Cheater!")
			}
		case "C":
			switch string(line[2]) {
			case "X":
				score += 2
			case "Y":
				score += 3 + 3
			case "Z":
				score += 1 + 6
			default:
				log.Fatal("Cheater!")
			}
		default:
			log.Fatal("Cheater!")
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	log.Println(score)

}
